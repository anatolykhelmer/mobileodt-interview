import { Uuid } from 'node-ts-uuid';

class Exam {
    id: string;
    patientId: string;
    summary: string;

    constructor(patientId: string) {
        this.id = Uuid.generate();
        this.patientId = patientId;
    }
}

export default Exam;