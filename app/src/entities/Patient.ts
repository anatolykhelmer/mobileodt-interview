import { Uuid } from 'node-ts-uuid';

class Patient {
    id: string;
    firstName: string;
    lastName: string;
    passportId: string;

    constructor() {
        this.id = Uuid.generate();
    }
}

export default Patient;