import * as express from "express";
import ExamRepository from "../../repositories/ExamRepository";
import Exam from "../../entities/Exam";

class ExamsController {
    public path = '/exams';
    public router = express.Router();
    private _repo = ExamRepository.getInstance();

    constructor() {
        this.initRoutes();
    }

    private initRoutes() {
        this.router.get(`${this.path}`, this.getAllExams);
        this.router.get(`${this.path}/:id`, this.getExam);
        this.router.post(`${this.path}`, this.createExam);
        this.router.put(`${this.path}/:id`, this.updateExam);
        this.router.delete(`${this.path}/:id`, this.deleteExam);
    }

    private getAllExams = (request: express.Request, response: express.Response) => {
        const patientId = request.param('patientId');
        if (patientId) {
            return response.status(200).json(this._repo.findByPatientId(patientId));
        }

        return response.status(200).json(Array.from(this._repo.getAll()));
    };

    private getExam = (request: express.Request, response: express.Response) => {
        const exam = this._repo.get(request.params.id);
        if (! exam) {
            return response.status(404).send();
        }

        return response.status(200).json(exam);
    };

    private deleteExam = (request: express.Request, response: express.Response) => {
        const exam = this._repo.get(request.params.id);
        if (! exam) {
            return response.status(404).send();
        }

        this._repo.delete(request.params.id);

        return response.status(200).send();
    };

    private updateExam = (request: express.Request, response: express.Response) => {
        const exam = this._repo.get(request.params.id);
        if (! exam) {
            return response.status(404).send();
        }

        exam.summary = request.body.summary;

        return response.status(200).json(exam);
    };

    private createExam = (request: express.Request, response: express.Response) => {
        const exam = new Exam(request.body.patientId);

        exam.summary = request.body.summary;

        this._repo.add(exam);

        return response.status(200).json(exam);
    };
}

export default ExamsController;