import * as express from "express";
import PatientRepository from "../../repositories/PatientRepository";
import ExamRepository from "../../repositories/ExamRepository";
import Patient from "../../entities/Patient";

class PatientsController {
    public path = '/patients';
    public router = express.Router();
    private _repo = PatientRepository.getInstance();
    private _examRepo = ExamRepository.getInstance();

    constructor() {
        this.initRoutes();
    }

    private initRoutes() {
        this.router.get(`${this.path}`, this.getAllPatients);
        this.router.get(`${this.path}/:id`, this.getPatient);
        this.router.post(`${this.path}`, this.createPatient);
        this.router.put(`${this.path}/:id`, this.updatePatient);
        this.router.delete(`${this.path}/:id`, this.deletePatient);
    }

    private getAllPatients = (request: express.Request, response: express.Response) => {
        return response.status(200).json(Array.from(this._repo.getAll()));
    };

    private getPatient = (request: express.Request, response: express.Response) => {
        const patient = this._repo.get(request.params.id);
        if (! patient) {
            return response.status(404).send();
        }

        const out: any = {};
        out.firstName = patient.firstName;
        out.lastName = patient.lastName;
        out.passportId = patient.passportId;
        out.exams = this._examRepo.findByPatientId(patient.id);

        return response.status(200).json(out);
    };

    private deletePatient = (request: express.Request, response: express.Response) => {
        const patient = this._repo.get(request.params.id);
        if (! patient) {
            return response.status(404).send();
        }

        this._repo.delete(request.params.id);

        return response.status(200).send();
    };

    private updatePatient = (request: express.Request, response: express.Response) => {
        const patient = this._repo.get(request.params.id);
        if (! patient) {
            return response.status(404).send();
        }

        patient.firstName = request.body.firstName;
        patient.lastName = request.body.lastName;
        patient.passportId = request.body.passportId;

        return response.status(200).json(patient);
    };

    private createPatient = (request: express.Request, response: express.Response) => {
        const patient = new Patient();
        patient.firstName = request.body.firstName;
        patient.lastName = request.body.lastName;
        patient.passportId = request.body.passportId;

        this._repo.add(patient);

        return response.status(200).json(patient);
    };
}

export default PatientsController;