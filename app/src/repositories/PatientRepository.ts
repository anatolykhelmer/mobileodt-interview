import Patient from "../entities/Patient";

class PatientRepository {
    private _repository: Map<string, Patient> = new Map<string, Patient>();
    private static _instance: PatientRepository;

    public static getInstance(): PatientRepository {
        if (! PatientRepository._instance) {
            PatientRepository._instance = new PatientRepository();
        }

        return PatientRepository._instance;
    }

    add(patient: Patient) {
        this._repository.set(patient.id, patient);
    }

    get(id: string) {
        return this._repository.get(id);
    }

    delete(id: string) {
        return this._repository.delete(id);
    }

    getAll() {
        return this._repository.values();
    }
}

export default PatientRepository;
