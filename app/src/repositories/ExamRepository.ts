import Exam from "../entities/Exam";

class ExamRepository {
    private _repository: Map<string, Exam> = new Map<string, Exam>();

    private static _instance: ExamRepository;

    public static getInstance(): ExamRepository {
        if (! ExamRepository._instance) {
            ExamRepository._instance = new ExamRepository();
        }

        return ExamRepository._instance;
    }

    add(exam: Exam) {
        this._repository.set(exam.id, exam);
    }

    get(id: string) {
        return this._repository.get(id);
    }

    findByPatientId(patientId: string) {
        return Array.from(this._repository.values()).filter(exam => exam.patientId == patientId);
    }

    delete(id: string) {
        return this._repository.delete(id);
    }

    getAll() {
        return this._repository.values();
    }
}

export default ExamRepository;