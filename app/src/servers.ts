import App from './app';
import PatientsController from './controllers/patients/patientsController';
import ExamsController from "./controllers/exams/examsController";

const app = new App(
    [
        new PatientsController(),
        new ExamsController()
    ],
    8080,
);

app.listen();
