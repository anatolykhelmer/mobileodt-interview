# MobileODT Interview Project
A platform for patient treatment.
Build on node.js 10 using express engine.
Uses in-memory persistence mode.

## Requirements
* docker (https://www.docker.com/)
* docker-compose (https://docs.docker.com/compose/)

## How to start
```bash
$ cd docker
$ ./start.sh
```

## How to stop
```bash
$ cd docker
$ ./stop.sh
```

## Testing
For testing purposes you can use the postman collection
"MobileODT Interview.postman_collection.json" in the project root directory.